package tree

// BinaryTree interface
type BinaryTree interface {
	IsEmpty() bool
	Insert(value int) (BinaryTree, error)
}
