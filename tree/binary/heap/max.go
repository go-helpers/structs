package heap

// Max ...
type Max struct {
	Min
}

// Less ...
func (h Max) Less(i, j int) bool { return h.Min[i] > h.Min[j] }
