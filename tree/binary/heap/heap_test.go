package heap_test

import (
	"encoding/json"
	"fmt"
	"structs/tree/binary/heap"
	"testing"
)

func Test_Min(t *testing.T) {
	m := &heap.Min{}

	heap.Push(m, 3)
	heap.Push(m, 1)
	heap.Push(m, 6)
	heap.Push(m, 2)
	heap.Push(m, 5)
	heap.Push(m, 4)
	fmt.Println(m)

	// heap.Pop(m)

	fmt.Println(m)

	// fmt.Println(heap.Inorder(m))
	fmt.Println("minsort", heap.Sort(m))

	fmt.Println("minsort", m)
}

func Test_Max(t *testing.T) {
	m := &heap.Max{}

	heap.Push(m, 3)
	heap.Push(m, 1)
	heap.Push(m, 6)
	heap.Push(m, 2)
	heap.Push(m, 5)
	heap.Push(m, 4)
	fmt.Println("max", m)

	// heap.Pop(m)

	fmt.Println(m)
	fmt.Println(heap.Sort(m))
}

func Test_StringOccurence(t *testing.T) {
	strings := []string{"apple", "banana", "apple", "grape", "pear", "banana", "apple"}

	mapper := make(map[string]int)
	for _, tmp := range strings {
		if _, ok := mapper[tmp]; ok {
			mapper[tmp]++
		} else {
			mapper[tmp] = 1
		}
	}
	// fmt.Println(mapper)

	queue := &heap.PriorityQueue{}
	for k, v := range mapper {
		tmp := heap.NewItem(v, k)
		heap.Push(queue, tmp)
	}
	o := heap.Sort(queue)
	j, _ := json.MarshalIndent(o, "", " ")
	fmt.Println(string(j))

}
