package heap

import (
	c "container/heap"
)

// Heap interface
type Heap interface {
	// Push(x interface{})
	// Len() int
	// Swap(i, j int)
	// Less(i, j int) bool
	// Pop() interface{}
	c.Interface
}

// Init establishes the heap invariants required by the other routines in this package.
// Init is idempotent with respect to the heap invariants
// and may be called whenever the heap invariants may have been invalidated.
// The complexity is O(n) where n = h.Len().
func Init(h Heap) {
	// heapify
	n := h.Len()
	for i := n/2 - 1; i >= 0; i-- {
		down(h, i, n)
	}
}

// Push pushes the element x onto the heap.
// The complexity is O(log n) where n = h.Len().
func Push(h Heap, x interface{}) {
	h.Push(x)
	up(h, h.Len()-1)
}

// Pop removes and returns the minimum element (according to Less) from the heap.
// The complexity is O(log n) where n = h.Len().
// Pop is equivalent to Remove(h, 0).
func Pop(h Heap) interface{} {
	n := h.Len() - 1
	h.Swap(0, n)
	down(h, 0, n)
	return h.Pop()
}

// Remove removes and returns the element at index i from the heap.
// The complexity is O(log n) where n = h.Len().
func Remove(h Heap, i int) interface{} {
	n := h.Len() - 1
	if n != i {
		h.Swap(i, n)
		if !down(h, i, n) {
			up(h, i)
		}
	}
	return h.Pop()
}

// Fix re-establishes the heap ordering after the element at index i has changed its value.
// Changing the value of the element at index i and then calling Fix is equivalent to,
// but less expensive than, calling Remove(h, i) followed by a Push of the new value.
// The complexity is O(log n) where n = h.Len().
func Fix(h Heap, i int) {
	if !down(h, i, h.Len()) {
		up(h, i)
	}
}

func up(h Heap, j int) {
	for {
		parent := (j - 1) / 2 // parent
		if parent == j || !h.Less(j, parent) {
			break
		}
		h.Swap(parent, j)
		j = parent
	}
}

func down(h Heap, i0, n int) bool {
	i := i0
	for {
		left := 2*i + 1
		right := left + 1
		if left >= n || left < 0 { // left < 0 after int overflow
			break
		}
		j := left // left child
		if right < n && h.Less(right, left) {
			j = right // = 2*i + 2  // right child
		}
		if !h.Less(j, i) {
			break
		}
		h.Swap(i, j)
		i = j
	}
	return i > i0
}

// Sort ...
func Sort(h Heap) (x []interface{}) {
	size := h.Len()
	for size > 0 {
		h.Swap(0, size-1)

		x = append(x, h.Pop())
		Init(h)
		size--
	}
	return
}
