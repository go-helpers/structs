package heap

// Item ..
type Item struct {
	Priority int
	Value    interface{}
}

// NewItem ...
func NewItem(priority int, value interface{}) *Item {
	return &Item{Priority: priority, Value: value}
}

var _ Heap = (*PriorityQueue)(nil)

// PriorityQueue ..
type PriorityQueue []*Item

// Len ...
func (me PriorityQueue) Len() int { return len(me) }

// Less ...
func (me PriorityQueue) Less(i, j int) bool {
	return me[i].Priority > me[j].Priority
}

// Pop ...
func (me *PriorityQueue) Pop() interface{} {
	old := *me
	x := old[len(old)-1]
	*me = old[:len(old)-1]
	return x
}

// Push ...
func (me *PriorityQueue) Push(x interface{}) {
	*me = append(*me, x.(*Item))
}

// Swap /.
func (me PriorityQueue) Swap(i, j int) {
	me[i], me[j] = me[j], me[i]
}
