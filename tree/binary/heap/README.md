# HEAP
- max heap 
- min heap 


## Properties
### Max Heap
- largest element stored at root 
- minimum element stored at leaves
- node cant have greater value than parent 

### Min Heap 
- smallest element stored at root 
- maximum element stored at leaves
- parent node cant have greater value than children



## Types of heaps 
- Binary heap 
- Fibionacci heap
- Binomial heap
- Pairing heap