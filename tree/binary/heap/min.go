package heap

import "fmt"

// Min max-heap
type Min []int

// Len returns length of Min heap
func (me Min) Len() int {
	return len(me)
}

// Push ...
func (me *Min) Push(i interface{}) {

	*me = append(*me, i.(int))
	return

}

// Pop ...
func (me *Min) Pop() interface{} {
	oldArray := *me
	oldArrayLengtth := len(oldArray)
	toPop := oldArray[oldArrayLengtth-1]
	fmt.Println(toPop)

	*me = oldArray[0 : oldArrayLengtth-1] //
	return toPop
}

// Less ...
func (me Min) Less(i, j int) bool {
	return me[i] < me[j]
}

// Swap ..
func (me Min) Swap(i, j int) {
	me[i], me[j] = me[j], me[i]
}
