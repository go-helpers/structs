package searchtree

import (
	"errors"
	"fmt"
	"sync"
)

var _ SearchTree = (*Basic)(nil)

// Basic ...
type Basic struct {
	lock sync.RWMutex
	Root *Node
}

// New returns a Basic binary tree
func New() *Basic {
	return new(Basic)
}

// IsEmpty ...
func (me *Basic) IsEmpty() bool {
	return me.Root == nil
}

// Insert ...
func (me *Basic) Insert(key int, value interface{}) (*Node, error) {
	me.lock.Lock()
	defer me.lock.Unlock()
	newNode := NewNode(key, value)
	if me.IsEmpty() {
		me.Root = newNode
		return me.Root, nil
	}
	if err := me.insertNode(me.Root, newNode); err != nil {
		return nil, err
	}
	return newNode, nil

}

func (me *Basic) insertNode(node *Node, newNode *Node) error {
	if node.Key == newNode.Key {
		return errors.New("key already exists")
	}

	// key must go left
	if newNode.Key < node.Key {
		if node.Left == nil {
			newNode.Parent = node
			node.Left = newNode
		} else {
			if err := me.insertNode(node.Left, newNode); err != nil {
				return err
			}
		}
	} else { // key must go right
		if node.Right == nil {
			newNode.Parent = node
			node.Right = newNode
		} else {
			if err := me.insertNode(node.Right, newNode); err != nil {
				return err
			}
		}
	}

	return nil
}

// FindMax ..
func (me *Basic) FindMax() *Node {
	if me.IsEmpty() {
		return nil
	}
	return me.Root.FindMax()
}

// Delete deletes key from tree
func (me *Basic) Delete(key int) (SearchTree, error) {
	// me.lock.Lock()
	// defer me.lock.Unlock()
	if me.IsEmpty() {
		return me, errors.New("empty tree")
	}
	if _, err := me.deleteNode(me.Root, key); err != nil { // _ is the root node
		return me, err
	}
	fmt.Println(me.Find(key))

	return me, nil
}

func (me *Basic) deleteNode(node *Node, key int) (*Node, error) {
	var err error
	switch {
	case key < node.Key:
		fmt.Println("k < n.Key...")
		node.Left, err = me.deleteNode(node.Left, key)
		return node, err
	case key > node.Key:
		fmt.Println("k > n.Key...")
		node.Right, err = me.deleteNode(node.Right, key)
		return node, err
	default:
		// fmt.Println("node has no children")
		if node.Left == nil && node.Right == nil {
			fmt.Println("nilly")
			node = nil
			return nil, nil
		}
		// fmt.Println("node only has right node")
		if node.Left == nil {
			fmt.Println("node only has right node")
			node.Right.Parent = node.Parent
			node = node.Right
			return node, nil
		}
		// fmt.Println("node only has left node")
		if node.Right == nil {
			fmt.Println("node only has left node")
			node.Left.Parent = node.Parent
			node = node.Left
			return node, nil
		}

		maxNode := node.Left.FindMax() // find the max node on the left
		fmt.Println("node", node)
		fmt.Println("smallest", maxNode, maxNode.Parent)

		if maxNode.Parent == node.Left { //
			node.Left.Left = maxNode.Left
			node.Left.Right = maxNode.Right
			node.Left.Parent = node.Parent

			maxNode.Parent = node.Parent
			maxNode.Right = node.Right

			maxNode.Left = node.Left
			*node = *maxNode
		} else { // node is parent of maxNode
			if node.Left == maxNode {
				maxNode.Left = nil
			}
			maxNode.Right = node.Right
			node.Right.Parent = maxNode

			*node = *maxNode

		}
		return node, nil
	}

	// return node, nil
}

// Find ...
func (me *Basic) Find(key int) (*Node, bool) {
	return me.Root.Find(key)
}

// Traverse ...
func (me *Basic) Traverse(t TraversalType, algo ...string) []interface{} {
	switch t {
	case TraversalInorder:
		return me.Root.Inorder(algo...)
	case TraversalPreorder:
	case TraversalPostorder:

	}
	return nil
}
