package searchtree_test

import (
	"fmt"
	"structs/tree/binary/searchtree"
	"testing"
)

func Test_NewBasic(t *testing.T) {
	tree := searchtree.New()
	tree.Insert(2, 2)
	tree.Insert(1, 1)
	tree.Insert(3, 3)
	tree.Insert(5, 5)
	fmt.Printf("root %+v\n", tree.Root)
	fmt.Printf("root->right node[%+v] parent[%+v]\n", tree.Root.Right, tree.Root.Right.Parent)
	fmt.Printf("root->right->right node[%+v] parent[%+v]\n", tree.Root.Right.Right, tree.Root.Right.Right.Parent)
}
func Test_NewBasicRemoval(t *testing.T) {
	tree := searchtree.New()
	tree.Insert(4, 4)
	tree.Insert(2, 2)
	tree.Insert(1, 1)
	tree.Insert(6, 6)

	tree.Insert(5, 5)
	tree.Insert(9, 9)

	fmt.Println("inorder", tree.Traverse(searchtree.TraversalInorder))
	fmt.Printf("root %+v\n", tree.Root)
	tree.Delete(6)
	fmt.Printf("root->right %+v %+v\n", tree.Root.Right, tree.Root.Right.Parent)
	fmt.Println("inorder", tree.Traverse(searchtree.TraversalInorder))

}

func Test_X(t *testing.T) {
	tree := searchtree.New()
	tree.Insert(10, 10)
	tree.Insert(8, 8)
	tree.Insert(20, 20)
	tree.Insert(15, 15)
	tree.Insert(12, 12)
	tree.Insert(11, 11)
	tree.Insert(14, 14)

	fmt.Println("inorder", tree.Traverse(searchtree.TraversalInorder))
	tree.Delete(20)
	fmt.Println("inorderxx", tree.Traverse(searchtree.TraversalInorder))
	// tree.Delete(8)
	// fmt.Println("inorderxx", tree.Traverse(searchtree.TraversalInorder))
	// tree.Delete(10)
	// fmt.Println("inorderxx", tree.Traverse(searchtree.TraversalInorder))

}
