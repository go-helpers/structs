package searchtree

// TraversalType ...
type TraversalType string

// Traversaltype
const (
	TraversalInorder   = TraversalType("IN")
	TraversalPreorder  = TraversalType("PRE")
	TraversalPostorder = TraversalType("POST")
)

// SearchTree ...
type SearchTree interface {
	Insert(key int, value interface{}) (*Node, error)
	Delete(key int) (SearchTree, error)
	Find(key int) (*Node, bool)
	FindMax() *Node
	IsEmpty() bool
	Traverse(TraversalType, ...string) []interface{}
}
