package redblacktree_test

import (
	"fmt"
	"structs/tree/binary/searchtree"
	redblacktree "structs/tree/binary/searchtree/redblacktree"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_NewRBTREE(t *testing.T) {
	tree := redblacktree.New()
	tree.Insert(2, "b")
	tree.Insert(1, "a")
	tree.Insert(3, "C")
	tree.Insert(5, "e")

	fmt.Println(tree.Traverse(searchtree.TraversalInorder))

	_, found := tree.Find(2)
	assert.True(t, found)
	_, found2 := tree.Find(12)
	assert.False(t, found2)

}
