package redblacktree

import (
	"structs/tree/binary/searchtree"
	"sync"
)

// RBTree ...
type RBTree interface {
	searchtree.SearchTree
	Balance(atNode *searchtree.Node)
}

// RedBlackTree ...
type RedBlackTree struct {
	*searchtree.Basic
}

var _ RBTree = (*RedBlackTree)(nil)

// New ...
func New() *RedBlackTree {
	return &RedBlackTree{
		Basic: searchtree.New(),
	}
}

// Balance ...
func (me *RedBlackTree) Balance(node *searchtree.Node) {
	var lock sync.RWMutex
	lock.Lock()
	defer lock.Unlock()

}

// Insert ...
func (me *RedBlackTree) Insert(key int, value interface{}) (*searchtree.Node, error) {
	node, err := me.Basic.Insert(key, value)
	if err != nil {
		return nil, err
	}
	me.Balance(node)
	return node, err
}
