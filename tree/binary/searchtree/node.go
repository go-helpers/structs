package searchtree

// Node ...
type Node struct {
	Key    int
	Value  interface{}
	Left   *Node
	Right  *Node
	Parent *Node
	Color  string
}

// NewNode ... initializes a new node
func NewNode(key int, value interface{}) *Node {
	return &Node{
		Key:   key,
		Value: value,
	}
}

// FindMax ...
func (me *Node) FindMax() *Node {
	if me == nil {
		return nil
	}
	if me.Right == nil {
		return me
	}
	return me.Right.FindMax()
}

// FindMin returns node with smallest key
func (me *Node) FindMin() *Node {
	if me == nil {
		return nil
	}
	if me.Left == nil {
		return me
	}
	return me.Left.FindMin()
}

// Find ..
func (me *Node) Find(key int) (*Node, bool) {
	if me == nil {
		return nil, false
	}
	if key == me.Key {
		return me, true
	}

	if key < me.Key {
		return me.Left.Find(key)
	}
	return me.Right.Find(key)
}

// MorrisTranversal is an Inorder returns a string
func (me *Node) morrisTranversal() (out []interface{}) {
	if me == nil {
		return
	}
	current := me
	for current != nil {
		if current.Left == nil {
			out = append(out, current.Value)
			current = current.Right
		} else {
			prev := current.Left
			for prev.Right != nil && prev.Right != current {
				prev = prev.Right
			}
			if prev.Right == nil {
				prev.Right = current
				current = current.Left
			} else {
				prev.Right = nil
				out = append(out, current.Value)
				current = current.Right
			}

		}
	}
	return
}

// Inorder ...
func (me *Node) Inorder(algo ...string) (out []interface{}) {
	if len(algo) == 0 {
		return me.morrisTranversal()
	}
	return nil
}

// MaxDepth ...
func (me *Node) MaxDepth() int {
	if me == nil {
		return 0
	}
	left, right := me.Left.MaxDepth(), me.Right.MaxDepth()

	if left > right {
		return left + 1
	}
	return right + 1
}
