package base

//NodeBinary basic Binary node
type NodeBinary struct {
	Left  *NodeBinary
	Right *NodeBinary
	Key   int
	V     interface{}
}

// NewBinaryNode ...
func NewBinaryNode(key int, value interface{}) *NodeBinary {
	return &NodeBinary{Key: key, V: value}
}

// Value ...
func (me *NodeBinary) Value() interface{} {
	return me.V
}

// Search searches for key and return
func (me *NodeBinary) Search(key int) (*NodeBinary, bool) {
	if me == nil {
		return nil, false
	}
	if key < me.Key {
		return me.Left.Search(key)
	}

	if key > me.Key {
		return me.Right.Search(key)
	}
	return me, true
}

// MorrisTranversal is an Inorder returns a string
func (me *NodeBinary) MorrisTranversal() (out []interface{}) {
	if me == nil {
		return
	}
	current := me
	for current != nil {
		if current.Left == nil {
			out = append(out, current.V)
			current = current.Right
		} else {
			prev := current.Left
			for prev.Right != nil && prev.Right != current {
				prev = prev.Right
			}
			if prev.Right == nil {
				prev.Right = current
				current = current.Left
			} else {
				prev.Right = nil
				out = append(out, current.V)
				current = current.Right
			}

		}
	}
	return
}
