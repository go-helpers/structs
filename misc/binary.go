package misc

import (
	"fmt"
	"strings"
)

// SentenceToBinary ...
func SentenceToBinary(s string) (binString string) {
	bin := []string{}
	for _, c := range s {

		bin = append(bin, fmt.Sprintf("%b", c))
	}
	return strings.Join(bin, " ")

}
