package stack_test

import (
	"structs/list/stack"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_List(t *testing.T) {
	stack := stack.NewSizedList(3)
	stack.Push(1, 1)
	stack.Push(2, 2)
	stack.Push(2, 2)
	// fmt.Println(stack.Top)
	assert.Equal(t, 3, stack.Size())
	pop1, _ := stack.Pop()
	assert.Equal(t, 2, pop1.Value)
	pop2, _ := stack.Pop()
	assert.Equal(t, 2, pop2.Value)
	pop3, _ := stack.Pop()
	assert.Equal(t, 1, pop3.Value)
	_, err := stack.Pop()
	assert.NotNil(t, err)

}
