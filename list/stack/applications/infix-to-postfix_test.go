package applications_test

import (
	"fmt"
	"structs/list/stack/applications"
	"testing"
)

func Test_InfixToPostfix(t *testing.T) {
	input := "a+b*(c^d-e)^(f+g*h)-i"
	fmt.Println(applications.InfixToPostfix(input))
}
