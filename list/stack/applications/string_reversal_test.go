package applications_test

import (
	"structs/list/stack/applications"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Reversal(t *testing.T) {
	x := "olla"
	assert.Equal(t, "allo", applications.Reverse(x))
}

func Test_ReverseWithStack(t *testing.T) {
	x := "olla"
	assert.Equal(t, "allo", applications.ReverseStack(x))

}
