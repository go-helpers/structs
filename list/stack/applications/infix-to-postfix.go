package applications

import (
	"fmt"
	"structs/list/stack"
	"unicode"
)

// InfixToPostfix converts an infix math equation to postfix
func InfixToPostfix(s string) (output string) {
	var (
		stack = stack.NewList()
		runez = []rune(s)
		out   = []rune{}
	)

	for _, tmp := range runez {
		if isOperand(tmp) {
			out = append(out, tmp)
		} else {
			p := precedence(tmp)
			if stack.IsEmpty() {
				stack.Push(p, tmp)
			}
			top, err := stack.Peek()
			if err != nil {
				continue
			}
			if p > top.Key {
				stack.Push(p, tmp)
			} else {
				for top != nil {
					fmt.Println("top", string(top.Value.(rune)))
					if top.Key >= p {
						out = append(out, top.Value.(rune))
						top, _ = stack.Pop()
					} else {
						break
					}

				}
				stack.Push(p, tmp)
			}

		}
	}
	top, _ := stack.Pop()
	for top != nil {
		out = append(out, top.Value.(rune))
		top, _ = stack.Pop()
	}

	return string(out)
}

func precedence(r rune) int {
	switch string(r) {
	case "^":
		return 3
	case "*", "/":
		return 2
	case "+", "-":
		return 1

	}
	return -1
}

func isOperand(r rune) bool {
	if unicode.IsDigit(r) {
		return true
	}

	if unicode.IsLetter(r) {
		return true
	}
	return false
}
