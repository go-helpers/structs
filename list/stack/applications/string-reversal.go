package applications

import "structs/list/stack"

// Reverse ...
func Reverse(input string) (output string) {
	var runic = []rune(input)

	for i := 0; i < len(runic)/2; i++ {
		runic[i], runic[len(runic)-1-i] = runic[len(runic)-1-i], runic[i]
	}
	return string(runic)
}

// ReverseStack ...
func ReverseStack(input string) string {
	var (
		stack  = stack.NewList()
		output string
		runic  = []rune(input)
	)
	for _, tmp := range runic {
		stack.Push(0, string(tmp))
	}
	for !stack.IsEmpty() {
		if top, err := stack.Peek(); err == nil {
			if tString, ok := top.Value.(string); ok {
				output += tString
			}
			stack.Pop()
		}
	}
	return output
}
