package stack

// Node ...
type Node struct {
	Next  *Node
	Key   int
	Value interface{}
}

// NewNode ...
func NewNode(k int, v interface{}) *Node {
	return &Node{
		Key:   k,
		Value: v,
	}
}

var _ Stack = (*SizedList)(nil)
var _ Stack = (*List)(nil)

// Stack ...
type Stack interface {
	IsEmpty() bool
	Push(int, interface{}) error
	Pop() (*Node, error)
	Peek() (*Node, error)
}
