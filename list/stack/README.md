# Stack 

## Characteristics
- LIFO 
- Stack overflow : if item i is pushed to a stack where max capacity is reached
- Stack underflow: attempt to pop an empty stack


## Operations 
- push(i) - inserts i at the top of stack 
- pop() - removes top element of stack
- top() - returns top element of stack
- size() - returns size of stack
- isEmpty() - returns true if stack is empty 

## Applications
### Converting base10 to base2
Consider the standard algorithm for converting a decimal number (base 10) into a binary number (base 2). This algorithm makes use of a single stack.

How many times will an item be pushed to this stack if the number to be converted is 10485961048596?

### Infix to PostFix