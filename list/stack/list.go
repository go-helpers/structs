package stack

import "errors"

// SizedList ...
type SizedList struct {
	List
	MaxSize int
}

// List ...
type List struct {
	Top *Node
}

// NewList reutrn snew list
func NewList() *List {
	return new(List)
}

// NewSizedList ...
func NewSizedList(maxSize int) *SizedList {
	return &SizedList{
		MaxSize: maxSize,
	}
}

// IsEmpty ...
func (me *List) IsEmpty() bool {
	return (me.Top == nil)
}

// Push ...
func (me *SizedList) Push(k int, i interface{}) error {
	if me.Size() == me.MaxSize {
		return errors.New("Overflow")
	}

	return me.List.Push(k, i)
}

// Push ...
func (me *List) Push(k int, i interface{}) error {
	newNode := NewNode(k, i)
	if me.IsEmpty() {
		me.Top = newNode
	} else {
		newNode.Next = me.Top
		me.Top = newNode
	}
	return nil

}

// Size ...
func (me *SizedList) Size() int {
	var (
		curr = me.Top
		size int
	)
	for curr != nil {
		size++
		curr = curr.Next
	}
	return size
}

// Pop ...
func (me *List) Pop() (*Node, error) {
	if me.Top == nil {
		return nil, errors.New("underflow")
	}
	v := me.Top
	me.Top = me.Top.Next
	return v, nil
}

// Peek ...
func (me *List) Peek() (*Node, error) {
	if me.IsEmpty() {
		return nil, errors.New("empty")
	}
	return me.Top, nil
}
