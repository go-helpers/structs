package stack

import "errors"

// Array linkedlist implementation of stack
type Array struct {
	top     int
	members []interface{}
}

// NewArray initializes a new Array
func NewArray() *Array {
	return &Array{
		top:     -1,
		members: make([]interface{}, 0),
	}
}

//IsEmpty returns true if the stack is empty
func (me *Array) IsEmpty() bool {
	return me.top == -1
}

// Pop removes top element of stack
func (me *Array) Pop() (*Array, error) {
	if me.IsEmpty() {
		return me, errors.New("emoty stack")
	}
	me.members = me.members[:me.top]
	me.top--

	return me, nil
}

// Push adds an element to the top of Stack
func (me *Array) Push(element interface{}) *Array {
	me.members = append(me.members, element)
	me.top++
	return me
}

// Top returns the value of the top element in stack
func (me *Array) Top() (interface{}, error) {
	if me.IsEmpty() {
		return nil, errors.New("stack undefined")
	}
	return me.members[me.top], nil
}
