package queue

// Queue ...
type Queue interface {
	Enqueue(interface{}) Queue
	Dequeue(interface{}) (Queue, error)
	IsEmpty() bool
	Front() interface{}
}
