package base

// BinaryNode ...
type BinaryNode interface {
	Search(key int) (BinaryNode, error)
}
