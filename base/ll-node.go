package base

// NodeLL loinked list node
type NodeLL struct {
	Next  *NodeLL
	Value interface{}
}
